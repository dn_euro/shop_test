<?php
	require_once('controllers/shop.php');
	Shop::getPageContent('admin_header');
	$shop = new Shop();
?>
<!-- WRAPPER -->
<div class="wrapper">

    <!-- CONTAINER -->
    <article class="container">
        <h3 class="text-center">— Product List —</h3>
        <div class="table-responsive">
            <table class="table shop_table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Material</th>
                        <th>Count</th>
                        <th>Image</th>
                        <th>Catalog</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                   <?php echo $shop->getAdminProdList('adminProdList');?>
                </tbody>
                <a href="#" id="js-new-prod">Add product</a>
            </table>
        </div>
    </article>
    <!-- /.container -->
</div>
<!-- /.wrapper -->

<?php 
	Shop::getPageContent('footer')
?>