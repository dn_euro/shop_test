<?php
	require_once('controllers/shop.php');
	$shop = new Shop();
	if (isset($_POST['del'])) {
		$shop->deleteProduct($_POST['ProductID']);
		header("Location: /admin_prod.php", TRUE, 301);
	}	
	if (isset($_POST['ProductID']) && !empty($_POST['ProductID'])) {		
		$shop->updateProduct($_POST);
	}
	if(isset($_GET['pid']) && $_GET['pid'] == 'new') {
		$product = array('Title' => 'New', 'Price' => '0', 'ProductID' => 'new');
		$buttons = '<input type="submit" value="Create">';
	} else {
		$product = $shop->getProduct($_GET['pid']);
		$product['Image'] = basename($product['Image']);
		$buttons = '<input type="submit" value="Update">&nbsp;&nbsp;<input type="submit" name="del" value="Delete">';
	}
	$catalogs = $shop->getCatalogTypes();
		foreach ($catalogs as $k => $v) {
			$product['catalogList'] .= '<option value="'.$v['CatalogID'].'" '.($product['CatalogID'] == $v['CatalogID'] ? 'selected' : '').'>'.$v['Title'].'</option>';
	}

	Shop::getPageContent('admin_header');
?>
<!-- WRAPPER -->
<div class="wrapper">

    <!-- CONTAINER -->
<form action="/admin_product_edit.php?pid=<?php echo $product['ProductID']?>" method="post">
	<input type="hidden" name="ProductID" value="<?php echo $product['ProductID']?>">
    <article class="container">
        <h3 class="text-center">— <?php echo $product['Title'];?> —</h3>
        <div class="table-responsive">
            <table class="table shop_table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
               		<tr>
                    	<td>Title</td>
                    	<td><input type="text" name="Title" value="<?php echo $product['Title']?>"></td>
                   	</tr>
                   	<tr>
                    	<td>CatalogTitle</td>
                    	<td><select name="CatalogID"><?php echo $product['catalogList']?></select></td>
                   	</tr>
                   	<tr>
                    	<td>Price</td>
                    	<td><input type="text" name="Price" value="<?php echo trim($product['Price'],'$');?>"></td>
                   	</tr>
                   <tr>
                    	<td>Material</td>
                    	<td><input type="text" name="Material" value="<?php echo $product['Material']?>"></td>
                   	</tr>
                   	<tr>
                    	<td>Count</td>
                    	<td><input type="text" name="Count" value="<?php echo $product['Count']?>"></td>
                   	</tr>
                   	<tr>
                    	<td>Description</td>
                    	<td><textarea name="Description"> <?php echo $product['Description']?></textarea></td>
                   	</tr>
					<tr>
                    	<td>Image</td>
                    	<td><input type="text" name="Image" value="<?php echo $product['Image']?>"></td>
                   	</tr>
                   	<tr>
                    	<td>Type</td>
                    	<td><input type="text" name="Extend[type]" value="<?php echo $product['Extend']['type']?>"></td>
                   	</tr>
                   	
                </tbody>
            </table>
            <br><?php echo $buttons ?>
        </div>
    </article>

</form>
    <!-- /.container -->
</div>
<!-- /.wrapper -->

<?php 
	Shop::getPageContent('footer')
?>