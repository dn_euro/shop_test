<?php
	require_once('controllers/shop.php');
	Shop::getPageContent('header');
	$shop = new Shop();
	if (isset($_GET['oid']) && !empty($_GET['oid'])) {
		$order = current($shop->getOrderInfo($_GET['oid']));
	} else {
		header("Location: /", TRUE, 301);
	}
?>
<script>
	for (var i in localStorage) {
			if (i.indexOf('prod') == 0) {
				localStorage.removeItem(i);
			}
		}
	localStorage.setItem('total_count', 0);
</script>
<!-- WRAPPER -->
<div class="wrapper">

    <!-- .page-header -->
    <header class="page-header container text-center">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="icon" data-icon="k"></div>
            <h1>— Checkout —</h1>
        </div>
    </header>
    <!-- /.page-header -->

    <!-- CONTAINER -->
    <article class="container text-center text-uppercase">
        <h3 class="text-muted">Thank you for your order</h3>
        <div class="order-info bg-primary clearfix">
            <div class="col-sm-2">
                <h4>order</h4>
                <p>#<?php echo $order['OrderID']; ?></p>
            </div>
            <div class="col-sm-3">
                <h4>date</h4>
                <p><?php echo date("F j, Y", strtotime($order['Date']));?></p>
            </div>
            <div class="col-sm-3">
                <h4>total</h4>
                <p><?php echo $shop->currency.' '.$order['Price'];?></p>
            </div>
            <div class="col-sm-4">
                <h4>PAYMENT METHOD</h4>
                <p>credit card</p>
            </div>
        </div>
    </article>
    <!-- /.container -->

    <!-- CONTAINER -->
    <article class="container text-center inforow">
        <div class="col-sm-6">
            <h4>BILLING information</h4>
            <address>
                <p>NAME: <?php echo $order['Username']?>
                    </p>
                <p>ADDRESS: <?php echo $order['Address']?></p>
                <p>EMAIL: <?php echo $order['Email']?></p>
            </address>
        </div>
     
    </article>
    <!-- /.container -->

</div>
<!-- /.wrapper -->
<?php 
	Shop::getPageContent('footer')
?>