-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:8889
-- Время создания: Окт 24 2016 г., 09:04
-- Версия сервера: 5.6.28
-- Версия PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `shop_test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `catalog`
--

CREATE TABLE `catalog` (
  `CatalogID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog`
--

INSERT INTO `catalog` (`CatalogID`, `Title`) VALUES
(1, 'Classic'),
(2, 'Modern'),
(3, 'Future');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `OrderID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Price` float NOT NULL,
  `Count` int(11) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Prods` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`OrderID`, `UserID`, `Price`, `Count`, `Date`, `Prods`) VALUES
(1, 5, 296, 4, '2016-10-23 19:57:22', '{"2":{"ProductID":"2","Title":"Farmhouse chair","totalCount":"2","Price":"59","totalPrice":"118"},"4":{"ProductID":"4","Title":"Outdoor chair","totalCount":"2","Price":"89","totalPrice":"178"}}'),
(2, 6, 1139, 4, '2016-10-24 04:47:59', '{"3":{"ProductID":"3","Price":"350","Title":"Plastic chair","totalPrice":1050,"totalCount":"3"},"4":{"ProductID":"4","Price":"89","Title":"Outdoor chair","totalPrice":89,"totalCount":"1"}}');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Price` float NOT NULL,
  `Material` varchar(255) NOT NULL,
  `Count` int(11) NOT NULL,
  `Description` tinytext NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Extend` varchar(255) NOT NULL,
  `CatalogID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`ProductID`, `Title`, `Price`, `Material`, `Count`, `Description`, `Image`, `Extend`, `CatalogID`) VALUES
(1, 'Contemporary chair', 100, 'wood/plastic', 4, '        Ea nec enim accumsan, ut prima blandit mel, labores nonumes detraxit an sed. Omnis malis propriae an sed, eu mea erat utinam meliore, inciderint philosophia usu ne. Laudem labores eu sed, vix in omnis habemus omnesque. TRATATA', 'cart-item-1.jpg', '{"type":"featured"}', 2),
(2, 'Farmhouse chair', 59, 'wood/material', 3, 'Omnis malis propriae an sed, eu mea erat utinam meliore, inciderint philosophia usu ne. Laudem labores eu sed, vix in omnis habemus omnesque.', 'cart-item-2.jpg', '{"type":"bestsellers"}', 1),
(3, 'Plastic chair', 350, 'plastic', 4, 'Ea nec enim accumsan, ut prima blandit mel, labores nonumes detraxit an sed. Omnis malis propriae an sed, eu mea erat utinam meliore, inciderint philosophia usu ne. Laudem labores eu sed, vix in omnis habemus omnesque.', 'cart-item-6.jpg', '{"type":"bestsellers"}', 2),
(4, 'Outdoor chair', 89, 'wood/material', 3, 'Omnis malis propriae an sed, eu mea erat utinam meliore, inciderint philosophia usu ne. Laudem labores eu sed, vix in omnis habemus omnesque.', 'cart-item-3.jpg', '{"type":"featured"}', 1),
(5, 'Classic style chair', 580, 'wood', 7, ' Ea nec enim accumsan, ut prima blandit mel, labores nonumes detraxit an sed. Omnis malis propriae an sed, eu mea erat utinam meliore, inciderint philosophia usu ne. Laudem labores eu sed, vix in omnis habemus omnesque.', 'cart-item-4.jpg', '{"type":"special"}', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`UserID`, `Username`, `Password`, `Address`, `Email`) VALUES
(1, 'Тест5', '', 'Харьков', 'dn@mail.ru'),
(2, 'test_d11', '', 'kharkov', 'asd#sda'),
(3, 'asdasd21312sda', '', 'asd', 'asd'),
(4, 'sdfsdasdas', '', 'sdf', 'dsf'),
(5, 'test2', '', 'Харьков', 'email2'),
(6, 'test2', '', 'Kharkov', 'qweqwe');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`CatalogID`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`OrderID`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductID`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `catalog`
--
ALTER TABLE `catalog`
  MODIFY `CatalogID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `OrderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `ProductID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
