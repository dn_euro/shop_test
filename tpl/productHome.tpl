<li class="col-md-3 col-sm-4 col-xs-6 product mix {Type}">
	<div class="product-img">
		<a href="/product.php?pid={ProductID}">
			<img src="{Image}" alt="">
		</a>
		<a href="#" data-id="prod{ProductID}" class="btn btn-primary add-cart">Add to Cart</a>
	</div>
	<h6><a href="product.html">{Title}</a></h6>
	<span class="price">
		<span class="amount">{Price}</span>
	</span>
</li>