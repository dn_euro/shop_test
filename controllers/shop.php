<?php 

class Shop {
	private $connect;
	private $basePath;
	private $imagePath;
	public $currency;
	public $totalPrice;
	public $totalCount;
	
	public function __construct() {
		$this->connect = new PDO('mysql:host=localhost:8889;dbname=shop_test', 'root', 'root');
		$this->basePath = $_SERVER["DOCUMENT_ROOT"];
		$this->imagePath = '/images/store/';
		$this->currency = '$';
	}
	
	public function __destruct() {
		$this->connect = null;
	}
	
	public function dbQuery($query) {
		if (!empty($query)) {
			$result = array();
			$q = $this->connect->query($query);
			while ($r = $q->fetch(PDO::FETCH_ASSOC)) {
				if(isset($r['ProductID'])){
					$result[$r['ProductID']] = $r;
				} else {
					$result[] = $r;
				}	
			}
			return $result;
		} else {
			return false;
		}
	}

	public static function getPageContent($name) {
		if (file_exists($name.'.php')) {
			echo file_get_contents($name.'.php');
		}
		return true;
	}
	
	private function getTpl($name) {
		if (file_exists($this->basePath.'/tpl/'.$name.'.tpl')) {
			return file_get_contents($this->basePath.'/tpl/'.$name.'.tpl');
		} else {
			return false;
		}
	}
	
	private function parseTpl($arr, $tpl) {
		$result = '';
		foreach ($arr as $k => $v) {
					$v['Extend'] = json_decode($v['Extend'], true);
					$v['Type'] = $v['Extend']['type'];
					$v['Image'] = $this->imagePath.$v['Image'];
					$v['Price'] = $this->currency.' '.$v['Price'];
					$temp = str_replace(array_keys($v), $v, $tpl);
					$result .= str_replace(array('{', '}'), array('',''), $temp);
				}
		return $result;
	}
		
	public function getCatalog($tplName = 'productRow', $arIds = array(), $type = '') {
		$where = '';
		$prodIds = array();
		$prodCount = array();
		
		$tpl = $this->getTpl($tplName);
		
		if(!empty($arIds)) {
			foreach ($arIds as $k => $v) {
				$k = str_replace('prod', '', $k);
				$prodCount[$k] = $v;
				$prodIds[] = $k;
			}
			$where = 'WHERE ProductID IN('.implode(',', $prodIds).')';
		}
		if (!empty($type)) {
			$where = empty($where) ? 'WHERE c.CatalogID='.$type : $where.' AND c.CatalogID='.$type;
		}
		
		$arProducts = $this->dbQuery('SELECT p.*, c.Title as CatalogName FROM products p LEFT JOIN catalog c ON p.CatalogID=c.CatalogID '.$where.' ORDER BY ProductID ASC');
		
		if (empty($arProducts) || empty($tpl)) {
			return false;
		}
		
		if (!empty($prodCount)) {
			foreach ($prodCount as $k => $v) {
				if (isset($arProducts[$k])) {
					$arProducts[$k]['Count'] = $v;
					$total = $v*$arProducts[$k]['Price'];
					$arProducts[$k]['Total'] = $this->currency.' '.$total;
					$this->totalPrice += $total;
					$this->totalCount += $v;
				}
			}
			
		}
		
		return $this->parseTpl($arProducts, $tpl);
	}
	
	public function getProduct($pid) {
		$arProdAttr = array();
		$arProdAttr = $this->dbQuery('SELECT p.*, c.Title as CatalogTitle FROM products p LEFT JOIN catalog c ON p.CatalogID=c.CatalogID where p.ProductID='.$pid);
		$arProdAttr = current($arProdAttr);
		$arProdAttr['Image'] = $this->imagePath.$arProdAttr['Image'];
		$arProdAttr['Extend'] = json_decode($arProdAttr['Extend'], true);
		$arProdAttr['Price'] = $this->currency.' '.$arProdAttr['Price'];
		return $arProdAttr;
	}
	
	public function setNewOrder($post){

		$query = 'INSERT INTO users (Username,Address,Email) VALUES (:name,:address,:email)';
		$q = $this->connect->prepare($query);
		$q->bindParam(':name', $post['name']);
		$q->bindParam(':address', $post['address']);
		$q->bindParam(':email', $post['email']);
		if ($q->execute()) {
			$lid = $this->connect->lastInsertId();
			$pid = array_keys($post['prods']);
			$arProd = $this->dbQuery('SELECT ProductID, Price, Title FROM products where ProductID IN('.implode(',', $pid).')');
			foreach ($arProd as $k => $v) {
				$arProd[$k]['totalPrice'] = ($v['Price']*$post['prods'][$k]);
				$arProd[$k]['totalCount'] = $post['prods'][$k];
				$this->totalPrice += ($v['Price']*$post['prods'][$k]);
				$this->totalCount += $post['prods'][$k];
 			}
			$query = 'INSERT INTO orders (UserID,Price,Count,Prods) VALUES (:UserID,:Price,:Count,:Prods)';
			$q = $this->connect->prepare($query);
			$q->bindParam(':UserID', $lid);
			$q->bindParam(':Price', $this->totalPrice);
			$q->bindParam(':Count', $this->totalCount);
			$q->bindParam(':Prods', json_encode($arProd));
			if($q->execute()) return $this->connect->lastInsertId();
		}
		return false;
		
	}
	
	public function getOrderInfo($oid) {
		$where = '';
		if(!empty($oid)) {
			$where = 'WHERE o.OrderID ='.$oid;
		}
		$arOrder = $this->dbQuery('SELECT o.*, u.* FROM orders o LEFT JOIN users u ON o.UserID=u.UserID '.$where.' ORDER BY o.OrderID DESC');
		if ($arOrder) {
			return $arOrder;
		}
		return false;
	}
	
	public function getAdminOrderInfo($tplName, $oid='') {
		$tpl = $this->getTpl($tplName);
		$arOrders = $this->getOrderInfo($oid);
		return $this->parseTpl($arOrders, $tpl);
	}
	
	public function getAdminProdList($tplName) {
		$arProds = $this->getCatalog($tplName);
		return $arProds;
	}
	
	public function updateOrder($order) {
		$query = 'UPDATE orders SET Price = :Price, Count = :Count, Prods = :Prods WHERE OrderID='.$order['OrderID'];
		$q = $this->connect->prepare($query);
		$q->bindParam(':Price', $order['Price']);
		$q->bindParam(':Count', $order['Count']);
		$q->bindParam(':Prods', json_encode($order['Prods']));
		if ($q->execute()) {
			$query = 'UPDATE users SET Username = :Username, Address = :Address, Email = :Email WHERE UserID='.$order['UserID'];
			$q = $this->connect->prepare($query);
			$q->bindParam(':Username', $order['Username']);
			$q->bindParam(':Address', $order['Address']);
			$q->bindParam(':Email', $order['Email']);
			return $q->execute();
		}
		return false;
	}
	
	public function getCatalogTypes() {
		return $this->dbQuery('SELECT * FROM catalog');
	}
	
	public function updateProduct($product) {
		$query =  'UPDATE products SET 
						Title = :Title, Price = :Price, Material = :Material,
						Count = :Count, Description = :Description, Image = :Image,
						Extend = :Extend, CatalogID = :CatalogID
					WHERE ProductID='.$product['ProductID'];
		
		if ($product['ProductID'] == 'new') {
			$query =  'INSERT INTO products 
						(Title, Price, Material, Count, Description, Image, Extend, CatalogID)
						VALUES 
						(:Title, :Price, :Material, :Count, :Description, :Image, :Extend, :CatalogID)';
		}
							
		$q = $this->connect->prepare($query);
		$q->bindParam(':Title', $product['Title']);
		$q->bindParam(':Price', trim($product['Price']));
		$q->bindParam(':Material', $product['Material']);
		$q->bindParam(':Count', $product['Count']);
		$q->bindParam(':Description', $product['Description']);
		$q->bindParam(':Image', $product['Image']);
		$q->bindParam(':CatalogID', $product['CatalogID']);
		$q->bindParam(':Extend', json_encode($product['Extend']));
		return $q->execute();
	}
	public function deleteProduct($productID) {
		return $this->connect->exec("DELETE FROM products WHERE ProductID=".$productID);
	}
	
	public function updateCatalog($catalog) {
		if (isset($catalog['Type']) && $catalog['Type'] == 'delete') {
			$this->connect->exec('DELETE FROM catalog WHERE CatalogID='.$catalog['CatalogID']);
		}
		$query =  'UPDATE catalog SET Title = :Title WHERE CatalogID='.$catalog['CatalogID'];
		
		if ($catalog['CatalogID'] == 'new') {
			$query =  'INSERT INTO catalog (Title) VALUES (:Title)';
		}
		$q = $this->connect->prepare($query);
		$q->bindParam(':Title', $catalog['Title']);
		return $q->execute();
	}
	
	
}

?>
