<?php
	require_once('controllers/shop.php');
	$shop = new Shop();
	Shop::getPageContent('header');
?>
<!-- WRAPPER -->
<div class="wrapper">

    <!-- .page-header -->
    <header class="page-header container text-center">
        <div class="col-sm-8 col-sm-offset-2">
            <h1>— chairs shop —</h1>
        </div>
    </header>
    <!-- /.page-header -->

    <!-- slider -->
    <section class="slider oneslider ecommerceslider text-uppercase">
        <div class="carouwrap clearfix">
            <ul>
                <li class="bg-sl-4">
                    <div class="container">
                        <div class="col-sm-5 col-md-offset-2 col-sm-offset-1 col-xs-8 col-xs-offset-2 divtable">
                            <div class="divcell">
                                <div class="e-name huge"><ins>50%</ins> off</div>
                                <p>Get a comfy seat all <br/> to yourself</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="bg-sl-5">
                    <div class="container">
                        <div class="col-sm-5 col-sm-offset-1 col-xs-8 col-xs-offset-2 divtable">
                            <div class="divcell">
                                <div class="e-name huge"><ins>2016</ins> catalogue</div>
                                <p>Where the wonderful everyday begins</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="bg-sl-6">
                    <div class="container">
                        <div class="col-sm-7 col-sm-offset-1 col-xs-8 col-xs-offset-2 divtable">
                            <div class="divcell">
                                <div class="e-name huge"><ins>NEW</ins> catalogue</div>
                                <p>Now available to view online </p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="slidebar">
                <a href="start_ecommerce.html#" class="arrow prev"><i></i></a>
                <a href="start_ecommerce.html#" class="arrow next"><i></i></a>
            </div>
        </div>
    </section>
    <!-- /.container -->

    <!-- CONTAINER -->
    <section class="container text-center">
        <nav>
            <ul class="nav-category">
                <li><a href="#all" class="filter" data-filter="all">All</a></li>
                <li><a href="#bestsellers" class="filter" data-filter=".bestsellers">bestsellers</a></li>
                <li><a href="#featured" class="filter" data-filter=".featured">featured</a></li>
                <li><a href="#special" class="filter" data-filter=".special">SPECIAL</a></li>
            </ul>
        </nav>

        <!-- mix-list -->
        <ul class="row mix-list">
           <?php echo $shop->getCatalog('productHome');?>
        </ul>
        <!-- /.mix-list -->
    </section>
    <!-- /.container -->

    <!-- RIBBON -->
    <article class="ribbon">
        <ul>
            <li class="col-md-3 col-xs-6">
                <img src="images/portfolio/work-1.jpg" alt="">
                <a href="javascript:void(0);" class="mask">
                    <h6>armchairs</h6>
                </a>
            </li>
            <li class="col-md-3 col-xs-6">
                <img src="images/portfolio/work-2.jpg" alt="">
                <a href="javascript:void(0);" class="mask">
                    <h6>wooden Chairs</h6>
                </a>
            </li>
            <li class="col-md-3 col-xs-6">
                <img src="images/portfolio/work-3.jpg" alt="">
                <a href="javascript:void(0);" class="mask">
                    <h6>bar Chairs</h6>
                </a>
            </li>
            <li class="col-md-3 col-xs-6">
                <img src="images/portfolio/work-4.jpg" alt="">
                <a href="javascript:void(0);" class="mask">
                    <h6>accessories</h6>
                </a>
            </li>
        </ul>
        <div class="slidebar">
            <div class="container">
                <a href="/catalog.php" class="block-link right">all <br/> categories</a>
            </div>
        </div>
    </article>
    <!-- /.ribbon -->

    <!-- CONTAINER -->
    <article class="container m-center">
        <div class="col-sm-4 text-center">
            <h3>about our shop — </h3>
        </div>
        <div class="col-sm-4">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        </div>
        <div class="col-sm-4">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        </div>
    </article>
    <!-- /.container -->

    <!-- SLIDER -->
    <article class="container slider logos text-center">
        <h2>— our brands —</h2>
        <div class="carouwrap clearfix">
            <div class="col-sm-10 col-sm-offset-1">
                <ul>
                    <li><a href="javascript:void(0)"><img src="images/logotypes/logo-1.jpg" alt=""></a></li>
                    <li><a href="javascript:void(0)"><img src="images/logotypes/logo-2.jpg" alt=""></a></li>
                    <li><a href="javascript:void(0)"><img src="images/logotypes/logo-3.jpg" alt=""></a></li>
                    <li><a href="javascript:void(0)"><img src="images/logotypes/logo-2.jpg" alt=""></a></li>
                    <li><a href="javascript:void(0)"><img src="images/logotypes/logo-1.jpg" alt=""></a></li>
                    <li><a href="javascript:void(0)"><img src="images/logotypes/logo-3.jpg" alt=""></a></li>
                    <li><a href="javascript:void(0)"><img src="images/logotypes/logo-3.jpg" alt=""></a></li>
                    <li><a href="javascript:void(0)"><img src="images/logotypes/logo-2.jpg" alt=""></a></li>
                    <li><a href="javascript:void(0)"><img src="images/logotypes/logo-1.jpg" alt=""></a></li>
                </ul>
            </div>
            <div class="slidebar">
                <a href="javascript:void(0)" class="arrow prev"><i></i></a>
                <a href="javascript:void(0)" class="arrow next"><i></i></a>
                <nav class="nav-pages"></nav>
            </div>
        </div>
    </article>
    <!-- /.slider -->
</div>
<!-- /.wrapper -->

<?php 
	Shop::getPageContent('footer')
?>