<?php
	require_once('controllers/shop.php');
	Shop::getPageContent('admin_header');
	$shop = new Shop();
	if (isset($_POST['Prods']) && !empty($_POST['Prods'])) {
		$shop->updateOrder($_POST);
	}
	$order = current($shop->getOrderInfo($_GET['oid']));
?>
<!-- WRAPPER -->
<div class="wrapper">

    <!-- CONTAINER -->
<form action="/admin_order_edit.php" method="post">
	<input type="hidden" name="OrderID" value="<?php echo $order['OrderID']?>">
	<input type="hidden" name="UserID" value="<?php echo $order['UserID']?>">
    <article class="container">
        <h3 class="text-center">— Orders <?php echo $order['OrderID']?> —</h3>
        <h5 class="text-center">— <?php echo $order['Date']?> —</h3>
        <div class="table-responsive">
            <table class="table shop_table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
               		<tr>
                    	<td>Customer</td>
                    	<td><input type="text" name="Username" value="<?php echo $order['Username']?>"></td>
                   	</tr>
                   	<tr>
                    	<td>Address</td>
                    	<td><input type="text" name="Address" value="<?php echo $order['Address']?>"></td>
                   	</tr>
                   <tr>
                    	<td>Email</td>
                    	<td><input type="text" name="Email" value="<?php echo $order['Email']?>"></td>
                   	</tr>
                   	<tr>
                    	<td>Count</td>
                    	<td><input type="text" name="Count" value="<?php echo $order['Count']?>"></td>
                   	</tr>
                   	<tr>
                    	<td>Price</td>
                    	<td><input type="text" name="Price" value="<?php echo $order['Price']?>"></td>
                   	</tr>
                   	<tr>
                    	<td>Products</td>
                    	<td>                    		
                    		<?php 	
                    				$table = '<table>
                    							<thead>
													<tr>
														<th>Title</th>
														<th>Count</th>
														<th>Price</th>
														<th>Total Price</th>
													</tr>
												</thead>
                    							';
                    				$prods = json_decode($order['Prods'], true);
                    				foreach ($prods as $prod) {
										$table .= '<tr>
														<input type="hidden" name="Prods['.$prod['ProductID'].'][ProductID]" value="'.$prod['ProductID'].'">
														<th><input type="text" name="Prods['.$prod['ProductID'].'][Title]" value="'.$prod['Title'].'"></th>
														<th><input type="text" name="Prods['.$prod['ProductID'].'][totalCount]" value="'.$prod['totalCount'].'"></th>
														<th><input type="text" name="Prods['.$prod['ProductID'].'][Price]" value="'.$prod['Price'].'"></th>
														<th><input type="text" name="Prods['.$prod['ProductID'].'][totalPrice]" value="'.$prod['totalPrice'].'"></th>
														
													</tr>';										                 		
                    				}
                    		
                    			$table .= '</table>';
                    			echo $table;
                    		?>
                    		
                    	</td>
                   	</tr>
                </tbody>
            </table>
            <br><input type="submit" value="Update">
        </div>
    </article>

</form>
    <!-- /.container -->
</div>
<!-- /.wrapper -->

<?php 
	Shop::getPageContent('footer')
?>