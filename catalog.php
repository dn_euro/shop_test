<?php
	require_once('controllers/shop.php');
	Shop::getPageContent('header');
	$shop = new Shop();
	$type = isset($_GET['t']) ? $_GET['t']: 1;
	
?>
<!-- WRAPPER -->
<div class="wrapper">

    <!-- .page-header -->
    <header class="page-header container text-center">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="icon" data-icon="a"></div>
            <h1>— Store —</h1>
            <h5>Chairs catalog</h5>
        </div>
    </header>
    <!-- /.page-header -->


    <!-- CONTAINER -->
    <article class="container products m-center">

	<?php echo $shop->getCatalog('productRow', array(), $type);?>
	
    </article>
    <!-- /.container -->
</div>
<!-- /.wrapper -->
<?php 
	Shop::getPageContent('footer')
?>