<?php
require_once('shop.php');
$shop = new Shop();
$result = '';
$totalPrice = 0;
$totalCount = 0;

if (isset($_POST['event']) && $_POST['event'] == 'getCatalogList') {
	$list = $shop->getCatalogTypes();
	foreach ($list as $v) {
		$result .= '<li><a href="/catalog.php?t='.$v['CatalogID'].'">'.$v['Title'].'</a></li>';
	}
	exit (json_encode(array('result' => $result)));
}

if (isset($_POST['CatalogID']) && !empty($_POST['CatalogID'])) {
	if ($shop->updateCatalog($_POST)) {
		$result = 'ok';
	}	
	exit (json_encode(array('result' => $result)));
}

if (isset($_POST['oid']) && !empty($_POST['oid'])) {
	$result = $shop->getAdminOrderInfo('adminOrderForm', $_POST['oid']);
	exit (json_encode(array('result' => $result)));
}

if (isset($_POST['arProd']) && !empty($_POST['arProd'])) {
	$result = $shop->getCatalog('productCartRow', $_POST['arProd']);
	$totalPrice = $shop->currency.' '.$shop->totalPrice;
	$totalCount = $shop->totalCount;
	exit (json_encode(array('result' => $result, 'totalPrice' =>  $totalPrice, 'totalCount' => $totalCount)));
}
