<?php
	require_once('controllers/shop.php');
	Shop::getPageContent('admin_header');
	$shop = new Shop();
?>
<!-- WRAPPER -->
<div class="wrapper">

    <!-- CONTAINER -->
    <article class="container">
        <h3 class="text-center">— Recent Orders —</h3>
        <div class="table-responsive">
            <table class="table shop_table">
                <thead>
                    <tr>
                        <th>Order</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>total</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    
                   <?php echo $shop->getAdminOrderInfo('adminOrderRow');?>
                </tbody>
            </table>
        </div>
    </article>
    <!-- /.container -->
</div>
<!-- /.wrapper -->

<?php 
	Shop::getPageContent('footer')
?>