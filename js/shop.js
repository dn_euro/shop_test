$(document).ready(function() {
	$('.add-cart').each(function(){
		var prod = $(this).data('id');
		if (localStorage.getItem(prod) !== null) {
			$(this).parent().find('input').val(localStorage.getItem(prod));
			$(this).html('Remove');
		}
	});
	
	if (localStorage.getItem('total_count') === null) {
		localStorage.setItem('total_count',0);
	} else {
		 getTotalLocalStorage();
	}
	
	$('.js-cart-count').html(localStorage.getItem('total_count'));
	
	if ($('#js-cart-wrapper').length > 0) {
		getCartItems();
	}
	
	$.post("/controllers/ajax.php",  {event:'getCatalogList'}, function(data) {
		$('#js-header-list-catalog').html(data.result);
	}, "json");
	
});

function getTotalLocalStorage(){
	total = 0;
		for (var i in localStorage) {
			if (i.indexOf('prod') == 0) {
				total = parseInt(total) + parseInt(localStorage[i]);
			}
		}
		localStorage.setItem('total_count',total);
		$('.js-cart-count').html(total);
}

function getCartItems() {
	
	$('.product-quantity input').each(function(){
		var prod = $(this).parent().data('id');
		localStorage.setItem(prod,$(this).val());
	});

	var arProd = {};
	for (var i in localStorage) {
		if (i.indexOf('prod') == 0) {
			arProd[i] = localStorage[i];
		}
	}
	if($.isEmptyObject(arProd)) {
		window.location = location.origin+'/cart_empty.php';
	}
		$.post("/controllers/ajax.php",  {arProd:arProd}, function(data) {
			$('#js-cart-wrapper').html(data.result);
			$('.js-total-price').html(data.totalPrice);
			localStorage.setItem('total_count',data.totalCount);
			$('.js-cart-count').html(data.totalCount);
		}, "json");
	
}

$(document).on('click', '.add-cart', function() {
	var prod = $(this).data('id'),
		count = $(this).parent().find('input').val(),
		total = localStorage.getItem('total_count');
	
	if (count === undefined) count = 1;
	if (localStorage.getItem(prod) === null) {
		localStorage.setItem(prod,count);
		$(this).html('Remove');
	} else {
		localStorage.removeItem(prod);
		$(this).parent().find('input').val(1)
		$(this).html('Add to card');
	}
	getTotalLocalStorage()
});


$(document).on('click', '.js-plus', function(e){
	e.preventDefault();
	var res = $(this).parent().find('input').val(),
		prod = $(this).parent().data('id');
	
	res++;
	$(this).parent().find('input').val(res);
	if (localStorage.getItem(prod) !== null) {
		getCartItems();
	}
	
	if ($('#js-cart-wrapper').length > 0) {
		getCartItems();
	}
});

$(document).on('click', '.js-minus', function(e){
	e.preventDefault();
	var res = $(this).parent().find('input').val(),
		prod = $(this).parent().data('id');
		
	if($(this).parent().find('input').val() != 1){
		res--;
		$(this).parent().find('input').val(res);
	} else{
		return false;
	}
	if (localStorage.getItem(prod) !== null) {
		getCartItems();
	}
	
	if ($('#js-cart-wrapper').length > 0) {
		getCartItems();
	}
});

$(document).on('change', '.product-quantity input', function(){
	
	if ($(this).val() <= 1){
		$(this).parent().find('.minus').addClass('disabled');
	} else {
		$(this).parent().find('.minus').removeClass('disabled');
	}
	
	getCartItems();	
});

$(document).on('click', '.remove', function(e){
	e.preventDefault();
	$(this).parents('tr').fadeOut(200, function(){
		$(this).remove();
		var prod = $(this).closest('tr').find('.product-quantity').data('id');
		localStorage.removeItem(prod);
		getCartItems();
		

	});
});

$(document).on('click', '.js-admin-order', function() {
	var oid = $(this).data('id');
	$.post("/controllers/ajax.php",  {oid:oid}, function(data) {
		$.colorbox({html:data.result});
		}, "json");
});

$(document).on('click', '#js-new-prod', function(e) {
	e.preventDefault();
	window.location=location.origin+'/admin_product_edit.php?pid=new'
});

$(document).on('click', '#js-new-catalog', function(e) {
	e.preventDefault();
	var tpl = '<tr>	<input type="hidden" name="CatalogID" value="new">\
					<th><input type="text" value="new" disabled></th>\
					<th><input type="text" name="Title" value=""></th>\
					<th><a href="#" onClick="submit()">Create</a></th></tr>';
	$('#js-catalog-list').append(tpl);
	$(this).hide();
	
});

$(document).on('click', '.js-update-catalog a', function(e) {
	e.preventDefault();
	var cid = $(this).closest('tr').data('id'),
		title = $(this).closest('tr').find('[name="Title"]').val(),
		type = $(this).data('type');
	$.post("/controllers/ajax.php",  {CatalogID:cid,Type:type,Title:title}, function(data) {
	if (data.result == 'ok') {
		window.location=location.origin+'/admin_catalog.php';
	}
	}, "json");
});


