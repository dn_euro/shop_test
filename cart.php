<?php
	require_once('controllers/shop.php');
	$shop = new Shop();
	if (isset($_POST['prods']) && !empty($_POST['prods'])) {
			$newOrderID = $shop->setNewOrder($_POST);
			if ($newOrderID > 0) {
				$location = 'Location: /thanks.php?oid='.$newOrderID;
				header($location, TRUE, 301);
			}
	};
	Shop::getPageContent('header');

?>
<!-- WRAPPER -->
<div class="wrapper">

    <!-- .page-header -->
    <header class="page-header container text-center">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="icon" data-icon="k"></div>
            <h1>— shopping cart —</h1>
        </div>
    </header>
    <!-- /.page-header -->

    <!-- CONTAINER -->
    <div id="cart">
		<form action="/cart.php" method="post">
		<article class="container text-center">
			<div class="col-sm-12">
				<div class="table-responsive">
					<table class="table shop_table cart text-center">
						<thead>
						<tr>
							<th></th>
							<th class="text-left">Product</th>
							<th class="text-center">price</th>
							<th class="text-center">QUANTITY</th>
							<th class="text-center">total</th>
							<th></th>
						</tr>
						</thead>
						<tbody id="js-cart-wrapper">
						</tbody>
					</table>
				</div>
			</div>
		</article>
    <!-- /.container -->

    <!-- CONTAINER -->
    	<article class="container no-padding-top inforow">
        <div class="col-md-6 col-sm-7 calculate">            
				<div class="form-group">
					<input type="text" name="name" placeholder="Name">
				</div>
				<div class="form-group">
					<input type="text" name="address" placeholder="Address">
				</div>
				<div class="form-group">
					<input type="text" name="email" placeholder="email">
				</div>
				<button type="submit" class="btn btn-primary btn-thn">CONFIRM</button>
			
        </div>
        <div class="col-sm-5">
            <table class="table cart-total">
                <tr>
                    <th>Cart Subtotal</th>
                    <td class="text-primary js-total-price"></td>
                </tr>
                <tr>
                    <th>Shipping and Handling</th>
                    <td class="text-primary">Free Shipping</td>
                </tr>
                <tr>
                    <th>Order Total</th>
                    <td class="text-primary js-total-price" ></td>
                </tr>
            </table>
        </div>
    </article>
	</form>
	</div>
	<!-- /.container -->

</div>
<!-- /.wrapper -->
<?php 
	Shop::getPageContent('footer')
?>