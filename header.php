<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <title>CHAIR SHOP</title>
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body class="home">

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<!-- HEADER -->
<header class="header">
    <div class="container">
    <div class="col-xs-3">
        <a href="/" class="logo">CHAIR SHOP</a>
    </div>
    <div class="col-xs-9">
        <!-- Mainmenu -->
        <nav class="navbar mainmenu">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <ul class="pull-right">
                <!-- Cart-list -->
                <li class="cart-list">
                    <a href="cart.php" class="dropdown-toggle"><img src="images/ico-cart.png" alt=""> cart <ins class="js-cart-count">0</ins></a>
                </li>
                <!-- /.cart-list -->
            </ul>
            <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
                <button type="button" class="pclose" data-toggle="collapse" data-target="#navbar-collapse"></button>
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a href="/" class="dropdown-toggle">Home</a>
                        
                    </li>
                    <li class="dropdown">
                        <a href="/catalog.php" class="dropdown-toggle" data-toggle="dropdown">Catalog</a>
                        <ul class="dropdown-menu" id="js-header-list-catalog">
                            <li><a href="/catalog.php?t=1">Classic</a></li>
                            <li><a href="/catalog.php?t=2">Modern</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- /.mainmenu -->
    </div>
    </div>
</header>
<!-- /.header -->

