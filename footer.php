
<!-- FOOTER -->
<footer class="footer">

   

    <!-- CONTAINER -->
    <div class="container text-center">
        <p>&copy; 2016 All Rights Reserved</p>
    </div>
    <!-- /.container -->
</footer>
<!-- /.footer -->


<!-- ScrollTop Button -->
<a href="start_ecommerce.html#" class="scrolltop"><i></i></a>

<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.plugins.js"></script>
<script src="js/custom.js"></script>
<script src="js/shop.js"></script>
<script src="js/jquery.colorbox-min.js"></script>
</body>
</html>