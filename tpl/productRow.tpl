<div class="product clearfix">
            <div class="col-sm-3">
                <a href="/product.php?pid={ProductID}">
                    <img src="{Image}" alt="">
                </a>
            </div>
            <div class="col-sm-9 cdescription">
                <div class="row">
                    <div class="col-md-9 col-sm-8">
                        <h4><a href="/product.php?pid={ProductID}">{Title} </a></h4>
                    </div>
                    <div class="col-md-3 col-sm-4 text-right">
                        <span class="price">
                            <ins>
                                <span class="amount">{Price}</span>
                            </ins>
                        </span>
                    </div>
                </div>

                <p>{Description}</p>
                <a href="catalog_list.html" data-count="1" data-id="prod{ProductID}" class="btn btn-primary btn-thn add-cart">Add to Cart</a>
            </div>
        </div>