<tr>
	<td class="product-thumbnail text-left">
		<a href="/product.php?pid={ProductID}"><img src="{Image}" alt=""></a>
	</td>
	<td class="product-name text-left">
		<a href="/product.php?pid={ProductID}">{Title}</a>
	</td>
	<td>
		<div class="product-price">{Price}</div>
	</td>
	<td class="product-quantity" data-id="prod{ProductID}">
		<a href="javascript:void(0);" class="js-minus">-</a>
		<input type="text" name="prods[{ProductID}]" value="{Count}">
		<a href="javascript:void(0);" class="js-plus">+</a>
	</td>
	<td class="product-subtotal">{Total}</td>
	<td class="product-remove text-center">
		<a href="#" class="remove"><img src="images/ico-close-black.png" alt=""></a>
	</td>
</tr>