<?php
	require_once('controllers/shop.php');
	$shop = new Shop();
	if (isset($_POST['CatalogID']) && $_POST['CatalogID'] == 'new') {
		var_dump($shop->updateCatalog($_POST));
		
	}
	
	$catalogTypes = $shop->getCatalogTypes();
	$catalogList = '';
	//var_dump($catalogTypes);
	foreach ($catalogTypes as $catalog) {
		$catalogList .= '<tr class="js-update-catalog" data-id="'.$catalog['CatalogID'].'">
							<th><input type="text" name="CatalogID" value="'.$catalog['CatalogID'].'" disabled></th>
							<th><input type="text" name="Title" value="'.$catalog['Title'].'"></th>
							<th><a href="#" data-type="update">Update</a> / <a href="#" data-type="delete">Delete</a></th>
						</tr>';	
	}
	Shop::getPageContent('admin_header');
?>
<!-- WRAPPER -->
<div class="wrapper">
	<form action="admin_catalog.php" method="post">
    <!-- CONTAINER -->
    <article class="container">
        <h3 class="text-center">— Catalog List —</h3>
        <div class="table-responsive">
            <table class="table shop_table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="js-catalog-list">
                   <?php echo $catalogList;?>
					<a href="#" id="js-new-catalog"> Add new</a>                   
                </tbody>
            </table>
        </div>
    </article>
    </form>
    <!-- /.container -->
</div>
<!-- /.wrapper -->

<?php 
	Shop::getPageContent('footer')
?>