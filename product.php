<?php
	if (!isset($_GET['pid']) || empty($_GET['pid'])) {
		header("Location: /", TRUE, 301);
	} 
	require_once('controllers/shop.php');
	Shop::getPageContent('header');
	$shop = new Shop();
	$product = $shop->getProduct($_GET['pid']);
	
?>
<!-- WRAPPER -->
<div class="wrapper">

    <!-- .page-header -->
    <header class="page-header container text-center">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="icon" data-icon="a"></div>
            <h1>— Store —</h1>
        </div>
    </header>
    <!-- /.page-header -->

    <!-- CONTAINER -->
    <article class="container type-product">
        <div class="col-sm-6 magnific-wrap">
            <div class="img-medium text-center">
                <div class="medium-slider">
                    <a href="<?php echo $product['Image']?>" title="<?php echo $product['Title']?>" class="magnific"><img src="<?php echo $product['Image']?>" alt=""></a>
                    <a href="<?php echo $product['Image']?>" title="<?php echo $product['Title']?>" class="magnific"><img src="<?php echo $product['Image']?>" alt=""></a>
                    <a href="<?php echo $product['Image']?>" title="<?php echo $product['Title']?>" class="magnific"><img src="<?php echo $product['Image']?>" alt=""></a>
                </div>
                <div class="product-count">1/3</div>
            </div>
        </div>
        <div class="col-sm-6 m-center">
            <h3><?php echo $product['Title']?></h3>
            <span class="price">
                <span class="amount"><?php echo $product['Price']?></span>
            </span>
            <div class="thumbs clearfix">
                <a href="<?php echo $product['Image']?>"><img  style="max-width:100px" src="<?php echo $product['Image']?>" alt=""></a>
                <a href="<?php echo $product['Image']?>"><img  style="max-width:100px" src="<?php echo $product['Image']?>" alt=""></a>
                <a href="<?php echo $product['Image']?>"><img  style="max-width:100px" src="<?php echo $product['Image']?>" alt=""></a>
            </div>
            <p><?php echo $product['Description']?></p>
            <div class="product-quantity inline" data-id="prod<?php echo $product['ProductID']?>">
                <a href="javascript:void(0);" class="js-minus">-</a>
                <input type="text" value="1">
                <a href="javascript:void(0);" class="js-plus">+</a>
            </div>
            <a class="btn btn-primary btn-thn add-cart" data-id="prod<?php echo $product['ProductID']?>" href="javascript:void(0);">Add to Cart</a>
            <table class="table cart-total">
                <tr>
                    <th>Id</th>
                    <td class="text-muted"><?php echo $product['CatalogTitle'].'_'.$product['ProductID']?></td>
                </tr>
                <tr>
                    <th>Category</th>
                    <td><?php echo $product['CatalogTitle']?></td>
                </tr>
                <tr>
                    <th>Type</th>
                    <td><?php echo ucfirst($product['Extend']['type'])?></td>
                </tr>
            </table>
        </div>
    </article>

</div>
<!-- /.wrapper -->

<?php 
	Shop::getPageContent('footer')
?>