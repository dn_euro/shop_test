<?php
	require_once('controllers/shop.php');
	Shop::getPageContent('header');

?>
<!-- WRAPPER -->
<div class="wrapper">

    <!-- .page-header -->
    <header class="page-header container text-center">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="icon" data-icon="k"></div>
            <h1>— shopping cart —</h1>
            <h5>Your cart is currently empty</h5>
        </div>
    </header>
    <!-- /.page-header -->

    <!-- CONTAINER -->
    <article class="container text-center">
        <p><a href="/" class="btn btn-default">Return to Shop</a></p>
    </article>
    <!-- /.container -->
</div>
<!-- /.wrapper -->
<?php 
	Shop::getPageContent('footer')
?>