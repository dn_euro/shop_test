<tr>
	<td class="order-number">#{OrderID}</td>
	<td class="order-status">{Username}</td>
	<td class="order-date">{Date}</td>	
	<td class="order-total">{Price} for {Count} item</td>
	<td class="order-actions text-right">
		<a href="/admin_order_edit.php?oid={OrderID}">View</a>
	</td>
</tr>